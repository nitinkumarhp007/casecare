package com.casecareapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.ClaimantProfileActivity;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.ReportModel;
import com.casecareapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ClaimantsListAdapter extends RecyclerView.Adapter<ClaimantsListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ClaimantModel> list;
    ArrayList<ClaimantModel> tempList;

    public ClaimantsListAdapter(Context context, ArrayList<ClaimantModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.claimant_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        // Glide.with(context).load(list.get(position).getId()).error(R.drawable.logo).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.ssn.setText("DOB: " + list.get(position).getDate());
        holder.claimant_num.setText("Claimant: #"+list.get(position).getId());
        holder.date.setText("Injury Date: "+list.get(position).getInjuryDate());
        holder.phone.setText("Phone: "+list.get(position).getContactNumber());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ClaimantProfileActivity.class);
                intent.putExtra("id", list.get(position).getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.ssn)
        TextView ssn;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.claimant_num)
        TextView claimant_num;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ClaimantModel> nList = new ArrayList<ClaimantModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ClaimantModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }

}
