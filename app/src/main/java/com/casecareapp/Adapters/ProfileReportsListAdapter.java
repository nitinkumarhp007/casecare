package com.casecareapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.casecareapp.Activities.AddReportActivity;
import com.casecareapp.ModelClasses.ReportModel;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProfileReportsListAdapter extends RecyclerView.Adapter<ProfileReportsListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ReportModel> list;

    public ProfileReportsListAdapter(Context context, ArrayList<ReportModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.reportlist_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.title.setText(list.get(position).getTitle());
        if (!list.get(position).getCreated().isEmpty())
            holder.date.setText("Date: " + util.convertTimeStampDate(Long.parseLong(list.get(position).getCreated())));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddReportActivity.class);
                intent.putExtra("is_edit", true);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.date)
        TextView date;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
