package com.casecareapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.AddDoctorActivity;
import com.casecareapp.Activities.AddReminderActivity;
import com.casecareapp.ModelClasses.DoctorModel;
import com.casecareapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileDoctorListAdapter extends RecyclerView.Adapter<ProfileDoctorListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<DoctorModel> list;

    public ProfileDoctorListAdapter(Context context, ArrayList<DoctorModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.doctorlist_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo).into(holder.profile_pic);

        holder.doctor_address.setText(list.get(position).getDoctor_address());

        if (!list.get(position).getEmail_address().isEmpty())
            holder.doctor_name.setText(list.get(position).getName() + " (" + list.get(position).getEmail_address() + ")");
        else
            holder.doctor_name.setText(list.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddDoctorActivity.class);
                intent.putExtra("is_edit", true);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile_pic)
        CircleImageView profile_pic;
        @BindView(R.id.doctor_name)
        TextView doctor_name;
        @BindView(R.id.doctor_address)
        TextView doctor_address;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
