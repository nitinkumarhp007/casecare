package com.casecareapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.AddNoteActivity;
import com.casecareapp.Activities.WebOpenActivity;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.StateModel;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.SavePref;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class StateListAdapter extends RecyclerView.Adapter<StateListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<StateModel> list;

    public StateListAdapter(Context context, ArrayList<StateModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.state_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

      /*  Glide.with(context).load(list.get(position).getImage()).into(holder.profile_pic);
       */


        holder.name.setText(list.get(position).getName());
        holder.link.setText(list.get(position).getUrl());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebOpenActivity.class);
                intent.putExtra("name", list.get(position).getName());
                intent.putExtra("url", list.get(position).getUrl());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.link)
        TextView link;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ClaimantModel> nList = new ArrayList<ClaimantModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ClaimantModel wp : tempList) {
                if (wp.getDescription().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/

}
