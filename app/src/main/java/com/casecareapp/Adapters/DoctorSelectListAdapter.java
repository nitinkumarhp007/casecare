package com.casecareapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.casecareapp.Activities.SelectDoctorActivity;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.DoctorModel;
import com.casecareapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DoctorSelectListAdapter extends RecyclerView.Adapter<DoctorSelectListAdapter.RecyclerViewHolder> {
    SelectDoctorActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<DoctorModel> list;
    ArrayList<DoctorModel> tempList;

    public DoctorSelectListAdapter(SelectDoctorActivity context, ArrayList<DoctorModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.doctor_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.doctor_name.setText(list.get(position).getName());

        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.selectdoctor(list.get(position).getId(), list.get(position).getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.doctor_name)
        TextView doctor_name;
        @BindView(R.id.select)
        TextView select;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<DoctorModel> nList = new ArrayList<DoctorModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (DoctorModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }


}
