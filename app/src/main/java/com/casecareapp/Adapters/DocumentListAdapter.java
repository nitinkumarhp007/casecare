package com.casecareapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.AddDocumentActivity;
import com.casecareapp.Activities.AddReminderActivity;
import com.casecareapp.Activities.ViewDocumentActivity;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.DocumentModel;
import com.casecareapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<DocumentModel> list;
    ArrayList<DocumentModel> tempList;

    public DocumentListAdapter(Context context, ArrayList<DocumentModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.document_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getImage()).into(holder.profile_pic);
        Glide.with(context).load(list.get(position).getDoc_image()).error(R.drawable.placeholder).into(holder.image);
        holder.name.setText(list.get(position).getClaimantname());
        holder.title.setText(list.get(position).getTitle());
        holder.description.setText(list.get(position).getDescription());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewDocumentActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.profile_pic)
        CircleImageView profile_pic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.image)
        ImageView image;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<DocumentModel> nList = new ArrayList<DocumentModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (DocumentModel wp : tempList) {
                if (wp.getTitle().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }

}
