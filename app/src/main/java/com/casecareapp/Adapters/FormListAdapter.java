package com.casecareapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.casecareapp.Activities.ClaimantProfileActivity;
import com.casecareapp.ModelClasses.ItemModel;
import com.casecareapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FormListAdapter extends RecyclerView.Adapter<FormListAdapter.RecyclerViewHolder> {
    ClaimantProfileActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ItemModel> list;

    public FormListAdapter(ClaimantProfileActivity context, ArrayList<ItemModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.form_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.text.setText(list.get(position).getText());

        if (list.get(position).isIs_selected())
            holder.view_offset_helper.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        else
            holder.view_offset_helper.setBackgroundColor(context.getResources().getColor(R.color.white));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               context.Select(position);
            }
        });



       /* Glide.with(context).load(list.get(position).getId()).error(R.drawable.logo).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.pickup_address.setText("R: " + list.get(position).getFrom_address());
        holder.delivery_address.setText("D:" + list.get(position).getTo_address());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.view_offset_helper)
        View view_offset_helper;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
