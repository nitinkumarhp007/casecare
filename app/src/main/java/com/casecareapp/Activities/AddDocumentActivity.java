package com.casecareapp.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.DocumentModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.Parser.PutMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddDocumentActivity extends AppCompatActivity {
    AddDocumentActivity context;
    private SavePref savePref;

    @BindView(R.id.title_text)
    EditText title_text;
    @BindView(R.id.select_claimant)
    TextView select_claimant;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.layout)
    RelativeLayout layout;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.video_play)
    ImageView video_play;
    @BindView(R.id.create)
    Button create;
    ProgressDialog mDialog = null;
    Uri fileUri;
    String claimant_id = "", selectedimage = "";
    boolean is_edit = false;


    DocumentModel documentModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_document);
        ButterKnife.bind(this);


        context = AddDocumentActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        setdata();

    }

    private void setdata() {
        is_edit = getIntent().getBooleanExtra("is_edit", false);

        if (is_edit) {

            util.hideKeyboard(context);

            documentModel = getIntent().getParcelableExtra("data");

            title_text.setText(documentModel.getTitle());
            description.setText(documentModel.getDescription());
            select_claimant.setText(documentModel.getClaimantname());
            claimant_id = documentModel.getClaimantId();
            Glide.with(context).load(documentModel.getDoc_image()).into(image);

            create.setText("Update");

        }


        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (!is_edit)
            title.setText("Create Document");
        else
            title.setText("Update Document");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.select_claimant, R.id.layout, R.id.create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.select_claimant:
                Intent intent1 = new Intent(context, SelectDoctorActivity.class);
                intent1.putExtra("type", "1");
                startActivityForResult(intent1, 200);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.create:
                TaskCreate();
                break;
            case R.id.layout:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;

        }
    }

    private void TaskCreate() {
        if (ConnectivityReceiver.isConnected()) {
            if (title_text.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Title");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (claimant_id.isEmpty()) {
                util.IOSDialog(context, "Please Select Claimant");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } /*else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Description");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }*/ else if (selectedimage.isEmpty()) {
                util.IOSDialog(context, "Please Uplaod Document");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (!is_edit)
                    ADDDOCUMENT_API();
                else
                    EDITDOCUMENT_API();

            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void ADDDOCUMENT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TITLE, title_text.getText().toString().trim());
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.CLAIMANTID, claimant_id);
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, "description");
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.ADDDOCUMENT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    private void EDITDOCUMENT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.ID, documentModel.getId());
        formBuilder.addFormDataPart(Parameters.TITLE, title_text.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CLAIMANTID, claimant_id);
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, "description");
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.EDITDOCUMENT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                    String title = "";
                    if (!is_edit)
                        title = "Document Added Successfully!";
                    else
                    {
                        util.is_doc_updated = true;
                        title = "Document Updated Successfully!";
                    }


                    util.hideKeyboard(context);

                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage(title).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    }).show();

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //checkPermissionOnActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 200) {
                if (data.getStringExtra("type").equals("1")) {
                    claimant_id = data.getStringExtra("id");
                    select_claimant.setText(data.getStringExtra("name"));
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(image);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }

        }
    }


    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


}