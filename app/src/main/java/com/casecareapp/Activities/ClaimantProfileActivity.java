package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.casecareapp.Adapters.ClaimantsListAdapter;
import com.casecareapp.Adapters.ProfileAppointmentListAdapter;
import com.casecareapp.Adapters.ProfileDoctorListAdapter;
import com.casecareapp.Adapters.FormListAdapter;
import com.casecareapp.Adapters.ProfileDocumentListAdapter;
import com.casecareapp.Adapters.ProfileNotesListAdapter;
import com.casecareapp.Adapters.ProfileReportsListAdapter;
import com.casecareapp.ModelClasses.AppointmentModel;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.DoctorModel;
import com.casecareapp.ModelClasses.DocumentModel;
import com.casecareapp.ModelClasses.ItemModel;
import com.casecareapp.ModelClasses.ReportModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ClaimantProfileActivity extends AppCompatActivity {
    ClaimantProfileActivity context;
    private SavePref savePref;

    @BindView(R.id.personal_information)
    RelativeLayout personal_information;
    @BindView(R.id.p1)
    LinearLayout p1;
    @BindView(R.id.p2)
    LinearLayout p2;
    @BindView(R.id.my_recycler_view_top)
    RecyclerView my_recycler_view_top;
    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.edit_button)
    ImageView edit_button;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.ssn)
    TextView ssn;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.claimant_num)
    TextView claimant_num;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.error_message)
    TextView error_message;

    @BindView(R.id.claimant_phone)
    TextView claimant_phone;
    @BindView(R.id.examiner)
    TextView examiner;
    @BindView(R.id.examiner_s_email)
    TextView examiner_s_email;
    @BindView(R.id.examiner_s_phone)
    TextView examiner_s_phone;

    @BindView(R.id.phone_click)
    LinearLayout phone_click;
    @BindView(R.id.examiner_s_email_click)
    LinearLayout examiner_s_email_click;
    @BindView(R.id.examiner_s_phone_click)
    LinearLayout examiner_s_phone_click;


    ClaimantModel claimantModel = null;

    ArrayList<ItemModel> item_list;
    FormListAdapter formListAdapter;

    ProgressDialog mDialog = null;
    private ArrayList<ClaimantModel> notes_list;
    private ArrayList<AppointmentModel> appointment_list;
    private ArrayList<DocumentModel> document_list;
    private ArrayList<DoctorModel> doctor_list;
    private ArrayList<ReportModel> report_list;

    String id = "";
    String name_text = "";
    String image_text = "";
    String dob_text = "";
    String ssn_text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claimant_profile);

        ButterKnife.bind(this);

        context = ClaimantProfileActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        id = getIntent().getStringExtra("id");


        personal_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (p1.getVisibility() == View.GONE) {
                    p1.setVisibility(View.VISIBLE);
                    p2.setVisibility(View.VISIBLE);
                } else {
                    p1.setVisibility(View.GONE);
                    p2.setVisibility(View.GONE);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddClaimantActivity.class);
                intent.putExtra("name", claimantModel.getName());
                intent.putExtra("phone", claimantModel.getContactNumber());
                intent.putExtra("ssn", claimantModel.getSsn());
                intent.putExtra("examiner_name", claimantModel.getExaminerName());
                intent.putExtra("examiner_contact_number", claimantModel.getExaminerContactNumber());
                intent.putExtra("examiner_email", claimantModel.getExaminerEmail());
                intent.putExtra("injuryDate", claimantModel.getInjuryDate());
                intent.putExtra("dob", claimantModel.getDate());
                intent.putExtra("image", claimantModel.getImage());
                intent.putExtra("claimant_id", claimantModel.getId());
                intent.putExtra("is_edit", true);
                startActivity(intent);
            }
        });

        phone_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call_Alert(claimantModel.getContactNumber());
            }
        });
        examiner_s_email_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!claimantModel.getExaminerEmail().isEmpty()) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + claimantModel.getExaminerEmail()));
                        intent.putExtra(Intent.EXTRA_SUBJECT, "");
                        intent.putExtra(Intent.EXTRA_TEXT, "");
                        startActivity(intent);
                    } finally {

                    }
                }

            }
        });
        examiner_s_phone_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call_Alert(claimantModel.getExaminerContactNumber());
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        CLAIMANTDETAIL();
    }

    private void setdata(ClaimantModel claimantModel) {
        //Glide.with(context).load(claimantModel.getImage()).error(R.drawable.logo).into(profile_pic);


        String first_latter = claimantModel.getName().substring(0, 1);
        TextDrawable drawable = TextDrawable.builder().beginConfig().withBorder(4).endConfig()
                .buildRoundRect(first_latter, getResources().getColor(R.color.colorPrimaryDark), 100);
        profile_pic.setImageDrawable(drawable);

        name.setText(claimantModel.getName());
        ssn.setText(claimantModel.getDate());
        claimant_num.setText("Claimant: #" + claimantModel.getId());
        date.setText(claimantModel.getInjuryDate());
        phone.setText(claimantModel.getContactNumber());
        claimant_phone.setText(claimantModel.getContactNumber());
        examiner.setText(claimantModel.getExaminerName());
        examiner_s_phone.setText(claimantModel.getExaminerContactNumber());
        examiner_s_email.setText(claimantModel.getExaminerEmail());


        layout.setVisibility(View.VISIBLE);


        item_list = new ArrayList<>();
        item_list.add(new ItemModel("Doctors", "", true));
        item_list.add(new ItemModel("Reports", "", false));
        item_list.add(new ItemModel("Notes", "", false));
        item_list.add(new ItemModel("Documents", "", false));
        item_list.add(new ItemModel("Appointments", "", false));

        formListAdapter = new FormListAdapter(context, item_list);
        my_recycler_view_top.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        my_recycler_view_top.setAdapter(formListAdapter);

        if (doctor_list.size() > 0) {
            my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
            my_recycler_view.setAdapter(new ProfileDoctorListAdapter(context, doctor_list));
            my_recycler_view.setVisibility(View.VISIBLE);
            error_message.setVisibility(View.INVISIBLE);
        } else {
            my_recycler_view.setVisibility(View.INVISIBLE);
            error_message.setVisibility(View.VISIBLE);
        }

    }

    public void Select(int position) {
        for (int i = 0; i < item_list.size(); i++) {
            item_list.get(i).setIs_selected(false);
        }
        item_list.get(position).setIs_selected(true);
        formListAdapter.notifyDataSetChanged();

        if (position == 0) {
            //Doctors
            if (doctor_list.size() > 0) {
                my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
                my_recycler_view.setAdapter(new ProfileDoctorListAdapter(context, doctor_list));
                my_recycler_view.setVisibility(View.VISIBLE);
                error_message.setVisibility(View.INVISIBLE);
            } else {
                my_recycler_view.setVisibility(View.INVISIBLE);
                error_message.setVisibility(View.VISIBLE);
            }
        } else if (position == 1) {
            //Reports
            if (report_list.size() > 0) {
                my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
                my_recycler_view.setAdapter(new ProfileReportsListAdapter(context, report_list));
                my_recycler_view.setVisibility(View.VISIBLE);
                error_message.setVisibility(View.INVISIBLE);
            } else {
                my_recycler_view.setVisibility(View.INVISIBLE);
                error_message.setVisibility(View.VISIBLE);
            }

        } else if (position == 2) {
            //Notes
            if (notes_list.size() > 0) {
                my_recycler_view.setLayoutManager(new GridLayoutManager(this, 2));
                my_recycler_view.setAdapter(new ProfileNotesListAdapter(context, notes_list));
                my_recycler_view.setVisibility(View.VISIBLE);
                error_message.setVisibility(View.INVISIBLE);
            } else {
                my_recycler_view.setVisibility(View.INVISIBLE);
                error_message.setVisibility(View.VISIBLE);
            }

        } else if (position == 3) {
            //Documents
            if (document_list.size() > 0) {
                my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
                my_recycler_view.setAdapter(new ProfileDocumentListAdapter(context, document_list));
                my_recycler_view.setVisibility(View.VISIBLE);
                error_message.setVisibility(View.INVISIBLE);
            } else {
                my_recycler_view.setVisibility(View.INVISIBLE);
                error_message.setVisibility(View.VISIBLE);
            }

        } else if (position == 4) {
            //Appointments
            if (appointment_list.size() > 0) {
                my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
                my_recycler_view.setAdapter(new ProfileAppointmentListAdapter(context, appointment_list));
                my_recycler_view.setVisibility(View.VISIBLE);
                error_message.setVisibility(View.INVISIBLE);
            } else {
                my_recycler_view.setVisibility(View.INVISIBLE);
                error_message.setVisibility(View.VISIBLE);
            }


        }
    }

    private void CLAIMANTDETAIL() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, id);
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.CLAIMANTDETAIL, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        doctor_list = new ArrayList<>();
        if (doctor_list.size() > 0)
            doctor_list.clear();

        appointment_list = new ArrayList<>();
        if (appointment_list.size() > 0)
            appointment_list.clear();

        notes_list = new ArrayList<>();
        if (notes_list.size() > 0)
            notes_list.clear();

        document_list = new ArrayList<>();
        if (document_list.size() > 0)
            document_list.clear();

        report_list = new ArrayList<>();
        if (report_list.size() > 0)
            report_list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONObject object = jsonMainobject.getJSONObject("body");
                    claimantModel = new ClaimantModel();

                    name_text = object.getString("name");
                    image_text = object.getString("image");
                    dob_text = object.getString("dob");
                    ssn_text = object.getString("ssn");

                    claimantModel.setContactNumber(object.getString("contactNumber"));
                    claimantModel.setId(object.getString("id"));
                    claimantModel.setImage(object.getString("image"));
                    claimantModel.setInjuryDate(object.getString("injuryDate"));
                    claimantModel.setName(object.getString("name"));
                    claimantModel.setSsn(object.getString("ssn"));
                    claimantModel.setDate(object.getString("dob"));
                    claimantModel.setExaminerName(object.getString(Parameters.EXAMINER_NAME));
                    claimantModel.setExaminerEmail(object.getString(Parameters.EXAMINER_EMAIL));
                    claimantModel.setExaminerContactNumber(object.getString(Parameters.EXAMINER_CONTACT_NUMBER));


                    JSONArray appointments = object.getJSONArray("appointments");
                    for (int i = 0; i < appointments.length(); i++) {
                        JSONObject appointments_obj = appointments.getJSONObject(i);
                        AppointmentModel appointmentModel = new AppointmentModel();
                        appointmentModel.setId(appointments_obj.getString("id"));
                        appointmentModel.setAddress(appointments_obj.getString("address"));
                        appointmentModel.setDate(appointments_obj.getString("date"));
                        appointmentModel.setDoctorId(appointments_obj.getString("doctorId"));
                        appointmentModel.setImage(appointments_obj.getJSONObject("doctor").optString("image"));
                        appointmentModel.setDoctorname(appointments_obj.getJSONObject("doctor").getString("name"));

                        appointmentModel.setC_id(id);
                        appointmentModel.setC_dob(dob_text);
                        appointmentModel.setC_image(image_text);
                        appointmentModel.setC_name(name_text);
                        appointmentModel.setC_user_id(id);
                        appointmentModel.setC_ssn(ssn_text);

                        appointmentModel.setTime(appointments_obj.getString("time"));
                        appointmentModel.setName(appointments_obj.getString("name"));
                        appointmentModel.setContactNumber(appointments_obj.getString("contactNumber"));
                        appointmentModel.setLatitude(appointments_obj.getString("latitude"));
                        appointmentModel.setLongitude(appointments_obj.getString("longitude"));
                        appointment_list.add(appointmentModel);
                    }

                    JSONArray notes = object.getJSONArray("notes");
                    for (int i = 0; i < notes.length(); i++) {
                        JSONObject notes_object = notes.getJSONObject(i);
                        ClaimantModel claimantModel1 = new ClaimantModel();
                        claimantModel1.setId(notes_object.getString("id"));
                        claimantModel1.setDate(notes_object.getString("date"));
                        claimantModel1.setDescription(notes_object.getString("description"));

                        claimantModel.setName(name_text);
                        claimantModel.setImage(image_text);
                        claimantModel.setUser_id(id);

                        notes_list.add(claimantModel1);
                    }

                    JSONArray documents = object.getJSONArray("documents");
                    for (int i = 0; i < documents.length(); i++) {
                        JSONObject document_object = documents.getJSONObject(i);
                        DocumentModel documentModel = new DocumentModel();
                        documentModel.setId(document_object.getString("id"));
                        documentModel.setTitle(document_object.getString("title"));
                        documentModel.setDescription(document_object.getString("description"));

                        documentModel.setClaimantname(name_text);
                        documentModel.setImage(image_text);
                        documentModel.setClaimantId(id);

                        document_list.add(documentModel);
                    }


                    JSONArray reports = object.getJSONArray("reports");
                    for (int i = 0; i < reports.length(); i++) {
                        JSONObject report_object = reports.getJSONObject(i);
                        ReportModel documentModel = new ReportModel();
                        documentModel.setId(report_object.getString("id"));
                        documentModel.setCreated(report_object.getString("created"));
                        documentModel.setTitle(report_object.optString(Parameters.TITLE));
                        documentModel.setDiagnosis(report_object.getString(Parameters.DIAGNOSIS));
                        documentModel.setAccepted_body_parts(report_object.getString(Parameters.ACCEPTEDBODYPARTS));
                        documentModel.setLitigation(report_object.getString(Parameters.LITIGATION));
                        documentModel.setGoals_for_referral(report_object.getString(Parameters.GOALSFORREFRAL));
                        documentModel.setProvider(report_object.getString(Parameters.PROVIDER));
                        documentModel.setMechanism_of_injury(report_object.getString(Parameters.MECHANISMOFINJURY));
                        documentModel.setSurgeries(report_object.getString(Parameters.SURGERIES));


                        documentModel.setClaimantname(name_text);
                        documentModel.setImage(image_text);
                        documentModel.setClaimantId(id);

                        report_list.add(documentModel);
                    }


                    JSONArray doctor = object.getJSONArray("doctors");
                    for (int i = 0; i < doctor.length(); i++) {
                        JSONObject doctor_object = doctor.getJSONObject(i);
                        DoctorModel doctorModel = new DoctorModel();
                        doctorModel.setId(doctor_object.getString("id"));
                        doctorModel.setName(doctor_object.getString("name"));
                        doctorModel.setDoctor_address(doctor_object.getString("address"));
                        doctor_list.add(doctorModel);
                    }


                    setdata(claimantModel);


                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void Call_Alert(String number) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to call?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

}