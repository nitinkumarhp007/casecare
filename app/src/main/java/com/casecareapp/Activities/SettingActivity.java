package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.casecareapp.MainActivity;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.Parser.PutMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.ligl.android.widget.iosdialog.BuildConfig;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SettingActivity extends AppCompatActivity {
    SettingActivity context;
    private SavePref savePref;

    @BindView(R.id.notification_switch)
    SwitchCompat notification_switch;
    @BindView(R.id.reset_app)
    LinearLayout reset_app;
    @BindView(R.id.feedback)
    LinearLayout feedback;
    @BindView(R.id.change_password)
    LinearLayout change_password;
    @BindView(R.id.email_us)
    LinearLayout email_us;
    @BindView(R.id.privacy_policy)
    LinearLayout privacy_policy;
    @BindView(R.id.share_this_app)
    LinearLayout share_this_app;
    @BindView(R.id.logout)
    Button logout;
    @BindView(R.id.logout_lay)
    RelativeLayout logout_lay;

    ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        context = SettingActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Settings");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.reset_app, R.id.feedback, R.id.change_password, R.id.email_us, R.id.privacy_policy, R.id.share_this_app, R.id.logout, R.id.logout_lay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.reset_app:
                ResetAppAlert();
                break;
            case R.id.feedback:
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "casecare@gmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback (CaseCare)");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //TODO smth
                }
                break;
            case R.id.change_password:
                startActivity(new Intent(context, ChangePasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.email_us:
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "casecare@gmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback (CaseCare)");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //TODO smth
                }
                break;
            case R.id.privacy_policy:

                break;
            case R.id.share_this_app:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.logout:
                Logout_Alert();
                break;
            case R.id.logout_lay:
                Logout_Alert();
                break;
        }
    }

    private void ResetAppAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Reset App? It won't be undone.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RESET_APP_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void Logout_Alert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LOGOUT_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    private void RESET_APP_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    util.showToast(context, "User Logout Successfully");
                    savePref.clearPreferences();
                    Intent intent = new Intent(context, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


}