package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.casecareapp.Adapters.AppointmentListAdapter;
import com.casecareapp.Adapters.StateListAdapter;
import com.casecareapp.ModelClasses.StateModel;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuidelinesActivity extends AppCompatActivity {
    GuidelinesActivity context;
    private SavePref savePref;

    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    ProgressDialog mDialog = null;

    ArrayList<StateModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guidelines);

        ButterKnife.bind(this);

        context = GuidelinesActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        add_data();

        setToolbar();

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Guidelines");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void add_data() {
        list = new ArrayList<>();

        list.add(new StateModel("Alabama", "https://labor.alabama.gov/wc/umbs.aspx"));
        list.add(new StateModel("Alaska", "http://www.legis.state.ak.us/basis/statutes.asp#23.30"));
        list.add(new StateModel("Arkansas", "http://www.awcc.state.ar.us/ruleind.html"));
        list.add(new StateModel("California", "https://www.dir.ca.gov/samples/search/querydwc.html"));
        list.add(new StateModel("Colorado", "https://www.colorado.gov/pacific/cdle/medical-treatment-guidelines"));
        list.add(new StateModel("Connecticut", "http://wcc.state.ct.us/law/menus/rel-stat-2017.html"));
        list.add(new StateModel("Delaware", "https://dia.delawareworks.com/workers-comp/documents/Rules%20of%20the%20Industrial%20Accident%20Board.pdf?20151116"));
        list.add(new StateModel("District of Columbia", "https://code.dccouncil.us/dc/council/code/titles/32/chapters/15/"));
        list.add(new StateModel("Florida", "https://www.myfloridacfo.com/Division/WC/PublicationsFormsManualsReports/StatutesRules/"));
        list.add(new StateModel("Georgia", "https://sbwc.georgia.gov/statutes-and-rules"));
        list.add(new StateModel("Hawaii", "http://labor.hawaii.gov/dcd/find-a-law/"));
        list.add(new StateModel("Idaho", "https://iic.idaho.gov/rules-and-legislation/"));
        list.add(new StateModel("Illinois", "https://www2.illinois.gov/sites/iwcc/Documents/Rules070610.pdf"));
        list.add(new StateModel("Indiana", "http://iga.in.gov/legislative/laws/2018/ic/titles/001"));
        list.add(new StateModel("Iowa", "http://publications.iowa.gov/2445/1/guidessixth.pdf"));
        list.add(new StateModel("Kansas", "https://www.dol.ks.gov/Files/PDF/kwc11.pdf"));
        list.add(new StateModel("Kentucky", "http://lrc.ky.gov/kar/803/025/010.html"));
        list.add(new StateModel("Louisiana", "www.laworks.net/Downloads/OWC/Ebilling_Rules.pdf"));
        list.add(new StateModel("Maine", " http://www.maine.gov/wcb/rules/current.html"));
        list.add(new StateModel("Maryland", "http://www.wcc.state.md.us/PDF/Regs/14.09.09.pdf"));
        list.add(new StateModel("Massachusetts", "https://www.mass.gov/files/documents/2017/10/30/452cmr1.pdf"));
        list.add(new StateModel("Michigan", "http://www.michigan.gov/wca/0,4682,7-191-26922_27470_28076-86583--,00.html"));
        list.add(new StateModel("Minnesota", "https://www.revisor.mn.gov/statutes/cite/176"));
        list.add(new StateModel("Mississippi", "https://www.mwcc.ms.gov/pdf/MWCCGeneralandProceduralRules4.pdf "));
        list.add(new StateModel("Missouri", "https://www.sos.mo.gov/adrules/csr/current/8csr/8csr.asp#8-50 "));
        list.add(new StateModel("Montana", "http://erd.dli.mt.gov/work-comp-regulations"));
        list.add(new StateModel("Nebraska", "https://nebraskalegislature.gov/laws/browse-chapters.php?chapter=48"));
        list.add(new StateModel("Nevada", "http://dir.nv.gov/WCS/Nevada_Law/"));
        list.add(new StateModel("New Jersey", "http://www.nj.gov/labor/wc/legal/legal_index.html"));
        list.add(new StateModel("New Mexico", "https://workerscomp.nm.gov/WCA-Rules-and-Statutes"));
        list.add(new StateModel("New York", "http://www.wcb.ny.gov/content/main/hcpp/MedicalTreatmentGuidelines/2014TreatGuide.jsp"));
        list.add(new StateModel("North Carolina", "http://www.ic.nc.gov/abtrules.html"));
        list.add(new StateModel("North Dakota", "https://www.legis.nd.gov/information/acdata/html/Title92.html"));
        list.add(new StateModel("Ohio", "https://www.bwc.ohio.gov/basics/rules"));
        list.add(new StateModel("Oklahoma", "http://cec.ok.gov/administrator_and_court_rules.html"));
        list.add(new StateModel("Oregon", "https://wcd.oregon.gov/laws/Pages/index.aspx"));
        list.add(new StateModel("Pennsylvania", "http://www.dli.pa.gov/Individuals/Workers-Compensation/publications/Documents/WC%20Act/wcact.pdf"));
        list.add(new StateModel("Rhode Island", "http://www.dlt.ri.gov/wc/lawsrules.html"));
        list.add(new StateModel("South Carolina", "https://www.scstatehouse.gov/coderegs/statmast.php"));
        list.add(new StateModel("South Dakota", "http://www.sdlegislature.gov/Statutes/Codified_Laws/DisplayStatute.aspx?Type=Statute&Statute=62"));
        list.add(new StateModel("Tennessee", "http://publications.tnsosfiles.com/rules/0800/0800-02/0800-02-06.20170129.pdf"));
        list.add(new StateModel("Texas", "http://texreg.sos.state.tx.us/public/readtac$ext.TacPage?sl=R&app=9&p_dir=&p_rloc=&p_tloc=&p_ploc=&pg=1&p_tac=&ti=28&pt=2&ch=134&rl=600"));
        list.add(new StateModel("Utah", "https://laborcommission.utah.gov/divisions/IndustrialAccidents/iadlaws.html"));
        list.add(new StateModel("Vermont", "http://labor.vermont.gov/workers-compensation/workers-compensation-rules"));
        list.add(new StateModel("Virginia", "https://law.lis.virginia.gov/vacode/title65.2/"));
        list.add(new StateModel("Washington", "http://www.lni.wa.gov/CLAIMSINS/PROVIDERS/TREATINGPATIENTS/TREATGUIDE/DEFAULT.ASP"));
        list.add(new StateModel("Washington", "http://www.lni.wa.gov/ClaimsIns/Files/SelfIns/ClaimMgt/MedTreat.pdf"));
        list.add(new StateModel("West Virginia", "http://www.wvinsurance.gov/Resources/Policy-Legislation/Workers-Comp-Rules"));
        list.add(new StateModel("Wisconsin", "https://dwd.wisconsin.gov/wc/councils/wcac/pdf/80_81_final_7_16_07.pdf"));
        list.add(new StateModel("Wyoming", "https://rules.wyo.gov/Search.aspx"));


        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new StateListAdapter(context, list));
    }
}