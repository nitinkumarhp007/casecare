package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.casecareapp.ModelClasses.ReportModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.Parser.PutMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddReportActivity extends AppCompatActivity {
    AddReportActivity context;
    private SavePref savePref;

    @BindView(R.id.create)
    Button create;
    @BindView(R.id.goals_for_referral)
    EditText goals_for_referral;
    @BindView(R.id.title_text)
    EditText title_text;
    @BindView(R.id.provider)
    EditText provider;
    @BindView(R.id.litigation)
    EditText litigation;
    @BindView(R.id.mechanism_of_injury)
    EditText mechanism_of_injury;
    @BindView(R.id.accepted_body_parts)
    EditText accepted_body_parts;
    @BindView(R.id.diagnosis)
    EditText diagnosis;
    @BindView(R.id.surgeries)
    EditText surgeries;
    @BindView(R.id.select_claimant)
    TextView select_claimant;

    ProgressDialog mDialog = null;

    String claimant_id = "";
    ReportModel reportModel = null;
    boolean is_edit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_report);
        ButterKnife.bind(this);


        context = AddReportActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        setdata();


    }

    private void setdata() {

        is_edit = getIntent().getBooleanExtra("is_edit", false);

        if (is_edit) {
            reportModel = getIntent().getParcelableExtra("data");

            select_claimant.setText(reportModel.getClaimantname());
            claimant_id = reportModel.getClaimantId();

            title_text.setText(reportModel.getTitle());
            goals_for_referral.setText(reportModel.getGoals_for_referral());
            provider.setText(reportModel.getProvider());
            litigation.setText(reportModel.getLitigation());
            mechanism_of_injury.setText(reportModel.getMechanism_of_injury());
            accepted_body_parts.setText(reportModel.getAccepted_body_parts());
            diagnosis.setText(reportModel.getDiagnosis());
            surgeries.setText(reportModel.getSurgeries());

            create.setText("Update");
        }


        setToolbar();
    }


    @OnClick({R.id.select_claimant, R.id.create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.select_claimant:
                Intent intent1 = new Intent(context, SelectDoctorActivity.class);
                intent1.putExtra("type", "1");
                startActivityForResult(intent1, 200);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.create:
                TaskCreate();
                break;

        }
    }

    private void TaskCreate() {
        if (ConnectivityReceiver.isConnected()) {
            if (claimant_id.isEmpty()) {
                util.IOSDialog(context, "Please Select Claimant");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (title_text.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Title");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (goals_for_referral.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Goals for referral");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (provider.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter provider");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (litigation.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Litigation");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (mechanism_of_injury.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Mechanism of injury");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (accepted_body_parts.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Accepted body parts");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (diagnosis.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Diagnosis");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (surgeries.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Surgeries");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (!is_edit)
                    ADDREPORT_API();
                else
                    EDITREPORT_API();
            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void ADDREPORT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CLAIMANTID, claimant_id);
        formBuilder.addFormDataPart(Parameters.TITLE, title_text.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.GOALSFORREFRAL, goals_for_referral.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PROVIDER, provider.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LITIGATION, litigation.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.MECHANISMOFINJURY, mechanism_of_injury.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ACCEPTEDBODYPARTS, accepted_body_parts.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DIAGNOSIS, diagnosis.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.SURGERIES, surgeries.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.ADDREPORT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    private void EDITREPORT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, reportModel.getId());
        formBuilder.addFormDataPart(Parameters.CLAIMANTID, claimant_id);
        formBuilder.addFormDataPart(Parameters.TITLE, title_text.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.GOALSFORREFRAL, goals_for_referral.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PROVIDER, provider.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LITIGATION, litigation.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.MECHANISMOFINJURY, mechanism_of_injury.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ACCEPTEDBODYPARTS, accepted_body_parts.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DIAGNOSIS, diagnosis.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.SURGERIES, surgeries.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.EDITREPORT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                    String title = "";
                    if (!is_edit)
                        title = "Report Added Successfully!";
                    else
                        title = "Report Updated Successfully!";

                    util.hideKeyboard(context);

                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage(title).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    }).show();

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //checkPermissionOnActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 200) {
                if (data.getStringExtra("type").equals("1")) {
                    claimant_id = data.getStringExtra("id");
                    select_claimant.setText(data.getStringExtra("name"));
                }
            }

        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (!is_edit)
            title.setText("Add Report");
        else
            title.setText("Update Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}