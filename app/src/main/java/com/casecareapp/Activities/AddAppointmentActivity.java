package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.MainActivity;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddAppointmentActivity extends AppCompatActivity {
    AddAppointmentActivity context;
    private SavePref savePref;

    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.doctor_name)
    TextView doctor_name;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.doctor_address)
    TextView doctor_address;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.type)
    TextView type;
    @BindView(R.id.create)
    Button create;

    ProgressDialog mDialog = null;
    private final int PLACE_PICKER_REQUEST = 787;
    String latitude = "", longitude = "", doctorId = "", claimant_id = "";
    Uri fileUri;
    private String selectedimage = "";

    private String current = "";
    private String ddmmyyyy = "MMDDYYYY";
    private Calendar cal = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment);

        ButterKnife.bind(this);
        context = AddAppointmentActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);




        setToolbar();
    }

    @OnClick({R.id.profile_pic, R.id.name,R.id.doctor_name, R.id.doctor_address, R.id.date, R.id.time, R.id.type, R.id.create})
    public void onViewClicked(View view) {
        util.hideKeyboard(context);
        switch (view.getId()) {
            case R.id.profile_pic:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.name:
                Intent intent1 = new Intent(context, SelectDoctorActivity.class);
                intent1.putExtra("type", "1");
                startActivityForResult(intent1, 200);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.doctor_name:
                Intent intent2 = new Intent(context, SelectDoctorActivity.class);
                intent2.putExtra("type", "2");
                startActivityForResult(intent2, 200);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.doctor_address:
                Locationget();
                break;
            case R.id.date:
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .curved()
                        .mustBeOnFuture()
                        .displayMinutes(false)
                        .displayHours(false)
                        .displayDays(true)
                        .displayMonth(true)
                        .displayYears(true)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date_text) {
                                Log.e("date___", date_text.toString());

                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
                                String dateTime = dateFormat1.format(date_text);
                                date.setText(dateTime);


                            }
                        })
                        .display();
                break;
            case R.id.time:
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .curved()
                        .mustBeOnFuture()
                        .displayMinutes(true)
                        .displayHours(true)
                        .displayDays(false)
                        .displayMonth(false)
                        .displayYears(false)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date_text) {
                                Log.e("date___", date_text.toString());

                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:MM");
                                String dateTime = dateFormat1.format(date_text);
                                time.setText(dateTime);


                            }
                        })
                        .display();
                break;
            case R.id.type:
                break;
            case R.id.create:
                TaskCreate();
                break;
        }
    }

    private void TaskCreate() {
        if (ConnectivityReceiver.isConnected()) {
           /* if (selectedimage.trim().isEmpty()) {
                util.IOSDialog(context, "Please select Image");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else*/ if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Claimant");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Contact Number");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (doctor_name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Doctor");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (doctor_address.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Doctor address");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (date.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Date");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (time.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Time");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } /*else if (type.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Type");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }*/ else {
                ADDAPPOINTMENT_API();
            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void ADDAPPOINTMENT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
    /*    if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }*/
        formBuilder.addFormDataPart(Parameters.DOCTORID, doctorId);
        formBuilder.addFormDataPart(Parameters.CLAIMANTID, claimant_id);
        formBuilder.addFormDataPart(Parameters.DATE, date.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.TIME, time.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CONTACTNUMBER, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ADDRESS, doctor_address.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.ADDAPPOINTMENT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    util.hideKeyboard(context);
                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage("Appointment Added Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    }).show();

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void Locationget() {
        // Initialize Places.
        Places.initialize(context, getString(R.string.api_key));
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(context);
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,
                com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS, com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.PHONE_NUMBER, com.google.android.libraries.places.api.model.Place.Field.RATING, com.google.android.libraries.places.api.model.Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(context);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //checkPermissionOnActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String placeName = String.valueOf(place.getName());
                doctor_address.setText(placeName);
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);

            } else if (requestCode == 200) {

                if (data.getStringExtra("type").equals("1")) {
                    claimant_id = data.getStringExtra("id");
                    name.setText(data.getStringExtra("name"));
                } else {
                    doctorId = data.getStringExtra("id");
                    doctor_name.setText(data.getStringExtra("name"));
                }


            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(profile_pic);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }

        }
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Create Appointment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}