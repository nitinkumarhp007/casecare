package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.casecareapp.MainActivity;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignInActivity extends AppCompatActivity {
    SignInActivity context;
    private SavePref savePref;

    @BindView(R.id.email_address)
    EditText email_address;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    Button forgot_password;
    @BindView(R.id.sign_in)
    Button sign_in;
    @BindView(R.id.sign_up)
    Button sign_up;

    ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        ButterKnife.bind(this);

        context = SignInActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

    }

    @OnClick({R.id.sign_in, R.id.forgot_password, R.id.sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                SigninProcess();
                break;
            case R.id.forgot_password:
                startActivity(new Intent(context, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.sign_up:
                startActivity(new Intent(context, SignUpActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (email_address.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                sign_in.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email_address.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                sign_in.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                sign_in.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, email_address.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "0");//0 => Android, 1 => Ios
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.USERLOGIN, formBody, "");
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    util.hideKeyboard(context);
                    JSONObject body = jsonMainobject.getJSONObject("body");
                    savePref.setAuthorization_key(body.getString("token"));
                    savePref.setID(body.getString("id"));
                    savePref.setName(body.optString("name"));
                    savePref.setEmail(body.getString("email"));
                    savePref.setImage(body.getString("image"));
                    savePref.setPhone(body.getString("phone"));

                    Intent intent = new Intent(context, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    util.showToast(context, "Login Sucessfully!!!");


                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}