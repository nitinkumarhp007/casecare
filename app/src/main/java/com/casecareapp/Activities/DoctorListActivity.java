package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.casecareapp.Adapters.ClaimantsListAdapter;
import com.casecareapp.Adapters.NotesListAdapter;
import com.casecareapp.Adapters.ProfileDoctorListAdapter;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.ModelClasses.DoctorModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DoctorListActivity extends AppCompatActivity {
    DoctorListActivity context;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.add)
    ImageView add;
    ArrayList<DoctorModel>list;

    ProgressDialog mDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);

        ButterKnife.bind(this);

        context = DoctorListActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DoctorListActivity.this, AddDoctorActivity.class));
            }
        });
        setToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();
        DOCTORLISTING_API();
    }

    private void DOCTORLISTING_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.DOCTORLISTING, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONArray data = jsonMainobject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        DoctorModel doctorModel = new DoctorModel();
                        doctorModel.setId(object.getString("id"));
                        doctorModel.setDoctor_address(object.getString("address"));
                        doctorModel.setEmail_address(object.getString("emailAddress"));
                        doctorModel.setFax_number(object.getString("faxNumber"));
                        doctorModel.setImage(object.getString("image"));
                        doctorModel.setName(object.getString("name"));
                        doctorModel.setPhone(object.getString("contactNumber"));
                        doctorModel.setLatitude(object.getString("latitude"));
                        doctorModel.setLogitude(object.getString("longitude"));
                        list.add(doctorModel);
                    }

                    if (list.size() > 0) {
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(new ProfileDoctorListAdapter(context,list));
                        myRecyclerView.setVisibility(View.VISIBLE);
                        error_message.setVisibility(View.GONE);
                    } else {
                        error_message.setVisibility(View.VISIBLE);
                        error_message.setText("No Doctor Found");
                        myRecyclerView.setVisibility(View.GONE);
                    }

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }



    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Doctors");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}