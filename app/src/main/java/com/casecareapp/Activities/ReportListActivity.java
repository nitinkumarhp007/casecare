package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.casecareapp.Adapters.DocumentListAdapter;
import com.casecareapp.Adapters.NotesListAdapter;
import com.casecareapp.Adapters.ReportListAdapter;
import com.casecareapp.ModelClasses.DocumentModel;
import com.casecareapp.ModelClasses.ReportModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ReportListActivity extends AppCompatActivity {
    ReportListActivity context;
    private SavePref savePref;

    @BindView(R.id.search_bar)
    EditText search_bar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.add)
    ImageView add;

    ArrayList<ReportModel>list;
    ReportListAdapter adapter=null;
    ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_list);
        ButterKnife.bind(this);

        context = ReportListActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        setToolbar();


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AddReportActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

        search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        REPORTLISTING_API();
    }

    private void REPORTLISTING_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.REPORTLISTING, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONArray data = jsonMainobject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        ReportModel documentModel = new ReportModel();
                        documentModel.setId(object.getString("id"));
                        documentModel.setCreated(object.getString("created"));
                        documentModel.setTitle(object.optString(Parameters.TITLE));
                        documentModel.setDiagnosis(object.getString(Parameters.DIAGNOSIS));
                        documentModel.setAccepted_body_parts(object.getString(Parameters.ACCEPTEDBODYPARTS));
                        documentModel.setLitigation(object.getString(Parameters.LITIGATION));
                        documentModel.setGoals_for_referral(object.getString(Parameters.GOALSFORREFRAL));
                        documentModel.setProvider(object.getString(Parameters.PROVIDER));
                        documentModel.setMechanism_of_injury(object.getString(Parameters.MECHANISMOFINJURY));
                        documentModel.setSurgeries(object.getString(Parameters.SURGERIES));
                        documentModel.setClaimantname(object.getJSONObject("claimant").getString("name"));
                        documentModel.setClaimantId(object.getJSONObject("claimant").getString("id"));
                        documentModel.setImage(object.getJSONObject("claimant").getString("image"));
                        list.add(documentModel);
                    }

                    if (list.size() > 0) {
                        adapter = new ReportListAdapter(context,list);
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(adapter);
                        myRecyclerView.setVisibility(View.VISIBLE);
                        error_message.setVisibility(View.GONE);
                    } else {
                        error_message.setVisibility(View.VISIBLE);
                        error_message.setText("No Reports Found");
                        myRecyclerView.setVisibility(View.GONE);
                    }
                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Reports");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}