package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.ModelClasses.DocumentModel;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewDocumentActivity extends AppCompatActivity {
    ViewDocumentActivity context;
    private SavePref savePref;

    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.select_claimant)
    TextView select_claimant;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.layout)
    RelativeLayout layout;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.video_play)
    ImageView video_play;
    @BindView(R.id.update)
    Button update;
    ProgressDialog mDialog = null;

    DocumentModel documentModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_document);

        ButterKnife.bind(this);

        context = ViewDocumentActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        video_play.setVisibility(View.INVISIBLE);

        setdata();

        setToolbar();

    }

    @OnClick({R.id.send_document, R.id.update, R.id.image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.send_document:
                Dialog();
                break;
            case R.id.update:
                util.is_doc_updated = false;
                Intent intent = new Intent(context, AddDocumentActivity.class);
                intent.putExtra("is_edit", true);
                intent.putExtra("data", documentModel);
                context.startActivity(intent);
                break;
            case R.id.image:
                Intent i= new Intent(context, ViewImageActivity.class);
                i.putExtra("image", documentModel.getDoc_image());
                context.startActivity(i);
                break;

        }
    }

    private void setdata() {
        documentModel = getIntent().getParcelableExtra("data");

        title_text.setText(documentModel.getTitle());
        description.setText(documentModel.getDescription());
        select_claimant.setText(documentModel.getClaimantname());
        Glide.with(context).load(documentModel.getDoc_image()).into(image);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (util.is_doc_updated) {
            util.is_doc_updated = false;
            finish();
        }

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Document");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void Dialog() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_send_inspection);

        EditText field_name = (EditText) dialog.findViewById(R.id.field_name);
        Button send = (Button) dialog.findViewById(R.id.send);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);

        dialog.show();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (field_name.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Email Address");
                } else if (!util.isValidEmail(field_name.getText().toString().trim())) {
                    util.IOSDialog(context, "Please Enter a Vaild Email Address");
                } else {
                    dialog.dismiss();
                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage("Document sent successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();

                    //SEND_EMAIL_API(field_name.getText().toString().trim());
                }

            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}