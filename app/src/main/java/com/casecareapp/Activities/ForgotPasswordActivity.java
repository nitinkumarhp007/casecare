package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.Parser.PutMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.util;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ForgotPasswordActivity extends AppCompatActivity {
    ForgotPasswordActivity context;
    @BindView(R.id.email_address)
    EditText email_address;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.submit)
    Button submit;

    ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);

        context = ForgotPasswordActivity.this;
        mDialog = util.initializeProgress(context);
    }
    @OnClick({R.id.back_button, R.id.submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.submit:
                if (ConnectivityReceiver.isConnected()) {
                    if (email_address.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Email Address");
                        submit.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                    } else if (!util.isValidEmail(email_address.getText().toString().trim())) {
                        util.IOSDialog(context, "Please Enter a Vaild Email Address");
                        submit.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                    } else {
                        FORGOTPASSWORD_API();

                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;

        }
    }

    private void FORGOTPASSWORD_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, email_address.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.FORGOT_PASSWORD, formBody, "");
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage("Please check your Email inbox").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    })
                            /* .setNegativeButton("Cancel", null)*/.show();


                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}