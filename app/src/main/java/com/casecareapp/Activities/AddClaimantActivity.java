package com.casecareapp.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.Parser.PutMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddClaimantActivity extends AppCompatActivity {
    AddClaimantActivity context;
    private SavePref savePref;

    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.ssn)
    EditText ssn;
    @BindView(R.id.date)
    EditText date;
    @BindView(R.id.dob)
    EditText dob;
    @BindView(R.id.examiner_name)
    EditText examiner_name;
    @BindView(R.id.examiner_contact_number)
    EditText examiner_contact_number;
    @BindView(R.id.examiner_email)
    EditText examiner_email;
    @BindView(R.id.create)
    Button create;

    Uri fileUri;
    private String selectedimage = "";
    ProgressDialog mDialog = null;
    private String current = "";
    private String ddmmyyyy = "MMDDYYYY";
    private Calendar cal = Calendar.getInstance();

    String injury_date_length = "";
    String dob_length = "",claimant_id="";
    boolean is_edit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_claimant);

        ButterKnife.bind(this);
        context = AddClaimantActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        is_edit = getIntent().getBooleanExtra("is_edit", false);


        date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ManualDate(s.toString(), false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ManualDate(s.toString(), true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setToolbar();
    }

    private void setdata() {
        if (is_edit) {
            Glide.with(context).load(getIntent().getStringExtra("image")).error(R.drawable.logo).into(profile_pic);
            name.setText(getIntent().getStringExtra("name"));
            ssn.setText(getIntent().getStringExtra("ssn"));
            if (!getIntent().getStringExtra("injuryDate").isEmpty())
                date.setText(getIntent().getStringExtra("injuryDate"));
            phone.setText(getIntent().getStringExtra("phone"));
            examiner_name.setText(getIntent().getStringExtra("examiner_name"));
            examiner_contact_number.setText(getIntent().getStringExtra("examiner_contact_number"));
            examiner_email.setText(getIntent().getStringExtra("examiner_email"));
            if (!getIntent().getStringExtra("dob").isEmpty())
                dob.setText(getIntent().getStringExtra("dob"));

            claimant_id=getIntent().getStringExtra("claimant_id");

            // CharSequence[] cs = String[] {"String to CharSequence"};

            //ManualDate(getIntent().getStringExtra("date"),true);
            //ManualDate(getIntent().getStringExtra("injuryDate"),false);


            create.setText("Update");

        }
    }


    @OnClick({R.id.profile_pic, R.id.date, R.id.dob, R.id.create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profile_pic:
                /*CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);*/
                break;
            case R.id.date:
              /*  util.hideKeyboard(context);
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .curved()
                        .mustBeOnFuture()
                        .displayMinutes(false)
                        .displayHours(false)
                        .displayDays(true)
                        .displayMonth(true)
                        .displayYears(true)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date_text) {
                                Log.e("date___", date_text.toString());

                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
                                String dateTime = dateFormat1.format(date_text);
                                date.setText(dateTime);


                            }
                        })
                        .display();*/
                break;
            case R.id.dob:
               /* util.hideKeyboard(context);
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .curved()
                        .mustBeOnFuture()
                        .displayMinutes(false)
                        .displayHours(false)
                        .displayDays(true)
                        .displayMonth(true)
                        .displayYears(true)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date_text) {
                                Log.e("date___", date_text.toString());

                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
                                String dateTime = dateFormat1.format(date_text);
                                dob.setText(dateTime);


                            }
                        })
                        .display();*/
                break;
            case R.id.create:
                TaskCreate();
                break;
        }
    }

    private void TaskCreate() {
        if (ConnectivityReceiver.isConnected()) {
            /*if (selectedimage.trim().isEmpty()) {
                util.IOSDialog(context, "Please select Image");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else*/
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (ssn.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Claim Number");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (date.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Injury Date");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!injury_date_length.equals("10")) {
                util.IOSDialog(context, "Please Enter Injury Date Correctly");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (dob.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Date of Birth");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!dob_length.equals("10")) {
                util.IOSDialog(context, "Please Enter Date of Birth Correctly");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (examiner_name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Examiner Name");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (examiner_contact_number.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Examiner Contact Number");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (examiner_email.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Examiner Email");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(examiner_email.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Examiner Email");
                create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            } else {
                if (!is_edit)
                    ADDCLAIMANT_API();
                else
                    EDITCLAIMANT_API();
            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void ADDCLAIMANT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CONTACTNUMBER, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.SSN, ssn.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EXAMINER_NAME, examiner_name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EXAMINER_CONTACT_NUMBER, examiner_contact_number.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EXAMINER_EMAIL, examiner_email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INJURYDATE, date.getText().toString().trim());// (format => MM/DD/YYYY)
        formBuilder.addFormDataPart(Parameters.DOB, dob.getText().toString().trim());// (format => MM/DD/YYYY)
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.ADDCLAIMANT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    private void EDITCLAIMANT_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.ID, claimant_id);
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CONTACTNUMBER, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.SSN, ssn.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EXAMINER_NAME, examiner_name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EXAMINER_CONTACT_NUMBER, examiner_contact_number.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EXAMINER_EMAIL, examiner_email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INJURYDATE, date.getText().toString().trim());// (format => MM/DD/YYYY)
        formBuilder.addFormDataPart(Parameters.DOB, dob.getText().toString().trim());// (format => MM/DD/YYYY)
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.EDITCLAIMANT, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                    String title = "";
                    if (is_edit)
                        title = "Claimant Updated Successfully!";
                    else
                        title = "Claimant Added Successfully!";

                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage(title).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    }).show();

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (!is_edit)
            title.setText("Add Claimant");
        else
            title.setText("Update Claimant");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        setdata();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                Glide.with(this).load(selectedimage).into(profile_pic);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private void ManualDate(String s, boolean is_dob) {
        if (!s.equals(current)) {
            String clean = s.replaceAll("[^\\d.]|\\.", "");
            String cleanC = current.replaceAll("[^\\d.]|\\.", "");

            int cl = clean.length();
            int sel = cl;
            for (int i = 2; i <= cl && i < 6; i += 2) {
                sel++;
            }
            //Fix for pressing delete next to a forward slash
            if (clean.equals(cleanC)) sel--;

            if (clean.length() < 8) {
                clean = clean + ddmmyyyy.substring(clean.length());
            } else {
                //This part makes sure that when we finish entering numbers
                //the date is correct, fixing it otherwise
                int mon = Integer.parseInt(clean.substring(0, 2));
                int day = Integer.parseInt(clean.substring(2, 4));
                int year = Integer.parseInt(clean.substring(4, 8));

                mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                cal.set(Calendar.MONTH, mon - 1);
                year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                cal.set(Calendar.YEAR, year);
                // ^ first set year for the line below to work correctly
                //with leap years - otherwise, date e.g. 29/02/2012
                //would be automatically corrected to 28/02/2012

                day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                clean = String.format("%02d%02d%02d", mon, day, year);
            }

            clean = String.format("%s/%s/%s", clean.substring(0, 2),
                    clean.substring(2, 4),
                    clean.substring(4, 8));

            sel = sel < 0 ? 0 : sel;
            Log.e("selllll", String.valueOf(sel));
            current = clean;
            if (!is_dob) {
                injury_date_length = String.valueOf(sel);
                date.setText(current);
                date.setSelection(sel < current.length() ? sel : current.length());
            } else {
                dob_length = String.valueOf(sel);
                dob.setText(current);
                dob.setSelection(sel < current.length() ? sel : current.length());
            }

        }
    }
}