package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.casecareapp.Adapters.DoctorSelectListAdapter;
import com.casecareapp.Adapters.ProfileDoctorListAdapter;
import com.casecareapp.ModelClasses.DoctorModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SelectDoctorActivity extends AppCompatActivity {
    SelectDoctorActivity context;

    @BindView(R.id.search_bar)
    EditText search_bar;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    private SavePref savePref;
    ArrayList<DoctorModel> list;
    ProgressDialog mDialog = null;
    String type = "";
    DoctorSelectListAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_doctor);

        ButterKnife.bind(this);
        context = SelectDoctorActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        type = getIntent().getStringExtra("type");


        setToolbar();
        if (type.equals("1"))
            CLAIMANTLISTING_API();
        else
            DOCTORLISTING_API();


        search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void DOCTORLISTING_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.DOCTORLISTING, formBody);
        getAsyncNew.hitApi();
    }

    private void CLAIMANTLISTING_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.CLAIMANTLISTING, formBody);
        getAsyncNew.hitApi();
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONArray data = jsonMainobject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        DoctorModel doctorModel = new DoctorModel();
                        doctorModel.setId(object.getString("id"));
                        doctorModel.setName(object.getString("name"));
                        list.add(doctorModel);
                    }

                    if (list.size() > 0) {
                        adapter = new DoctorSelectListAdapter(context, list);
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(adapter);
                        myRecyclerView.setVisibility(View.VISIBLE);
                        error_message.setVisibility(View.GONE);
                    } else {
                        error_message.setVisibility(View.VISIBLE);
                        error_message.setText("No Data Found");
                        myRecyclerView.setVisibility(View.GONE);
                    }

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void selectdoctor(String id, String name) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("id", id);
        returnIntent.putExtra("type", type);
        returnIntent.putExtra("name", name);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (type.equals("1"))
            title.setText("Select Claimant");
        else
            title.setText("Select Doctor");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}