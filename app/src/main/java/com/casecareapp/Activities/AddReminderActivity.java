package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.Parser.PutMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.ConnectivityReceiver;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddReminderActivity extends AppCompatActivity {
    AddReminderActivity context;
    private SavePref savePref;

    @BindView(R.id.title_text)
    EditText title_text;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.create)
    Button create;

    boolean is_edit = false;
    ClaimantModel claimantModel = null;
    ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);
        ButterKnife.bind(this);

        context = AddReminderActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        is_edit = getIntent().getBooleanExtra("is_edit", false);

        if (is_edit) {
            claimantModel = getIntent().getParcelableExtra("data");

            title_text.setText(claimantModel.getName());
            date.setText(claimantModel.getDate());
            description.setText(claimantModel.getDescription());

            create.setText("Update");
        }


        setToolbar();
    }

    @OnClick({R.id.date, R.id.create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.date:
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .curved()
                        .mustBeOnFuture()
                        .displayMinutes(true)
                        .displayHours(true)
                        .displayDays(true)
                        .displayMonth(true)
                        .displayYears(true)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date_text) {
                                Log.e("date___", date_text.toString());

                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy HH:MM");
                                String dateTime = dateFormat1.format(date_text);
                                date.setText(dateTime);


                            }
                        })
                        .display();
                break;
            case R.id.create:
                TaskCreate();
                break;
        }
    }


    private void TaskCreate() {
        if (ConnectivityReceiver.isConnected()) {
            if (title_text.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Title");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (date.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Date");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Description");
                create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (!is_edit)
                    ADDREMINDER_API();
                else
                    EDITREMINDER_API();
            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void ADDREMINDER_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TITLE, title_text.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DATETIME, date.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.ADDREMINDER, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    private void EDITREMINDER_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, claimantModel.getId());
        formBuilder.addFormDataPart(Parameters.TITLE, title_text.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DATETIME, date.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        PutMethod getAsyncNew = new PutMethod(context, AllAPIS.EDITREMINDER, formBody, savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                    String title = "";
                    if (!is_edit)
                        title = "Reminder Added Successfully!";
                    else
                        title = "Reminder Updated Successfully!";

                    util.hideKeyboard(context);

                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage(title).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    }).show();

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (!is_edit)
            title.setText("Add Reminder");
        else
            title.setText("Update Reminder");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}