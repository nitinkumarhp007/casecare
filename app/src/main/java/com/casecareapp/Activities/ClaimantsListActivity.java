package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.casecareapp.Adapters.ClaimantsListAdapter;
import com.casecareapp.Adapters.NotesListAdapter;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ClaimantsListActivity extends AppCompatActivity {
    ClaimantsListActivity context;
    private SavePref savePref;
    @BindView(R.id.search_bar)
    EditText search_bar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.add)
    ImageView add;

    ProgressDialog mDialog = null;
    private ArrayList<ClaimantModel> list;
    ClaimantsListAdapter adapter=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claimants_list);
        ButterKnife.bind(this);

        context = ClaimantsListActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClaimantsListActivity.this, AddClaimantActivity.class));
            }
        });


        search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setToolbar();


    }

    @Override
    protected void onResume() {
        super.onResume();
        CLAIMANTLISTING_API();
    }

    private void CLAIMANTLISTING_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.CLAIMANTLISTING, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONArray data = jsonMainobject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        ClaimantModel claimantModel = new ClaimantModel();
                        claimantModel.setContactNumber(object.getString("contactNumber"));
                        claimantModel.setId(object.getString("id"));
                        claimantModel.setImage(object.getString("image"));
                        claimantModel.setInjuryDate(object.getString("injuryDate"));
                        claimantModel.setName(object.getString("name"));
                        claimantModel.setSsn(object.getString("ssn"));
                        claimantModel.setDate(object.getString("dob"));
                        claimantModel.setExaminerName(object.getString(Parameters.EXAMINER_NAME));
                        claimantModel.setExaminerContactNumber(object.getString(Parameters.EXAMINER_CONTACT_NUMBER));
                        claimantModel.setExaminerEmail(object.getString(Parameters.EXAMINER_EMAIL));
                        list.add(claimantModel);
                    }

                    if (list.size() > 0) {
                        adapter = new ClaimantsListAdapter(context, list);
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(adapter);
                        myRecyclerView.setVisibility(View.VISIBLE);
                        error_message.setVisibility(View.GONE);
                    } else {
                        error_message.setVisibility(View.VISIBLE);
                        error_message.setText("No Claimant Found");
                        myRecyclerView.setVisibility(View.GONE);
                    }

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Claimants");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}