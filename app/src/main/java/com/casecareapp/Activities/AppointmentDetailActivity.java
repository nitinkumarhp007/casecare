package com.casecareapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.ModelClasses.AppointmentModel;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.SavePref;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppointmentDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    AppointmentDetailActivity context;
    private SavePref savePref;

    @BindView(R.id.back)
    ImageView back_button;
    @BindView(R.id.appointment_num)
    TextView appointment_num;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.dob)
    TextView dob;
    @BindView(R.id.ssn)
    TextView ssn;
    @BindView(R.id.claimant_num)
    TextView claimant_num;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.doctor_name)
    TextView doctor_name;
    @BindView(R.id.take_me_there)
    Button take_me_there;

    AppointmentModel appointmentModel;
    private float currentZoom = 12;
    private GoogleMap mMap;

    Marker marker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_detail);

        ButterKnife.bind(this);
        context = AppointmentDetailActivity.this;
        savePref = new SavePref(context);

        setdata();
    }

    @OnClick({R.id.back, R.id.take_me_there})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.take_me_there:
                if (appointmentModel.getLatitude() != null) {
                    if (!appointmentModel.getLatitude().isEmpty()) {
                        loadNavigationView(appointmentModel.getLatitude()
                                , appointmentModel.getLongitude());
                    }
                }
                break;
        }

    }

    private void setdata() {
        appointmentModel = getIntent().getParcelableExtra("data");


        appointment_num.setText("Appointment #" + appointmentModel.getId());
        date.setText(appointmentModel.getDate());
        time.setText(appointmentModel.getTime());

        name.setText(appointmentModel.getC_name());
        dob.setText("Phone: " + appointmentModel.getC_dob());
        ssn.setText("SSN: " + appointmentModel.getC_ssn());
        claimant_num.setText("Claim: #" + appointmentModel.getC_id());
        Glide.with(context).load(appointmentModel.getC_image()).into(profile_pic);

        location.setText(appointmentModel.getAddress());
        doctor_name.setText(appointmentModel.getDoctorname());

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.proselectlocationmapid);
        mapFragment.getMapAsync(this);

    }

    public void loadNavigationView(String lat, String lng) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", Double.parseDouble(lat) + "," + Double.parseDouble(lng));
        String url = builder.build().toString();
        Log.e("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url + "&mode=driving"));
        startActivity(i);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (appointmentModel.getLatitude() != null) {
            if (!appointmentModel.getLatitude().isEmpty()) {
                LatLng latLng1 = new LatLng(Double.parseDouble(appointmentModel.getLatitude()),
                        Double.parseDouble(appointmentModel.getLongitude()));

                marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng1)
                        .draggable(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_3x)));
                marker.setTag(999);


                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        latLng1, currentZoom);
                Log.e("ZOOM__", String.valueOf(currentZoom));
                mMap.animateCamera(location, 2100, null);
            }
        }
    }
}