package com.casecareapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.AddAppointmentActivity;
import com.casecareapp.Activities.AddNoteActivity;
import com.casecareapp.Adapters.AppointmentListAdapter;
import com.casecareapp.Adapters.NotesListAdapter;
import com.casecareapp.Adapters.ProfileDoctorListAdapter;
import com.casecareapp.ModelClasses.AppointmentModel;
import com.casecareapp.ModelClasses.DoctorModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.Parser.PostMethod;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }

    Context context;
    private SavePref savePref;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.wish)
    TextView wish;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.add)
    ImageView add;
    Unbinder unbinder;

    ArrayList<AppointmentModel> list;

    ProgressDialog mDialog = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AddAppointmentActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });
        setcalender(view);



        return view;
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    private void APPOINTMENTLISTING_API(String date) {
        Log.e("date_selected", date);
        util.hideKeyboard(getActivity());
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DATE, date);//yyyy-mm-dd
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.APPOINTMENTLISTING, formBody,savePref.getAuthorization_key());
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        Log.e("date_selected", event.getMessage());
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONArray data = jsonMainobject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        AppointmentModel appointmentModel = new AppointmentModel();
                        appointmentModel.setId(object.getString("id"));
                        appointmentModel.setAddress(object.getString("address"));
                        appointmentModel.setDate(object.getString("date"));
                        appointmentModel.setDoctorId(object.getString("doctorId"));
                        appointmentModel.setImage(object.optString("image"));
                        appointmentModel.setDoctorname(object.getJSONObject("doctor").getString("name"));

                        appointmentModel.setC_id(object.getJSONObject("claimant").getString("id"));
                        appointmentModel.setC_dob(object.getJSONObject("claimant").getString("contactNumber"));
                        appointmentModel.setC_image(object.getJSONObject("claimant").getString("image"));
                        appointmentModel.setC_name(object.getJSONObject("claimant").getString("name"));
                        appointmentModel.setC_user_id(object.getJSONObject("claimant").getString("userId"));
                        appointmentModel.setC_ssn(object.getJSONObject("claimant").getString("ssn"));

                        appointmentModel.setTime(object.getString("time"));
                        appointmentModel.setName(object.getString("name"));
                        appointmentModel.setContactNumber(object.getString("contactNumber"));
                        appointmentModel.setLatitude(object.getString("latitude"));
                        appointmentModel.setLongitude(object.getString("longitude"));
                        list.add(appointmentModel);
                    }

                    if (list.size() > 0) {
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(new AppointmentListAdapter(context, list));
                        myRecyclerView.setVisibility(View.VISIBLE);
                        error_message.setVisibility(View.GONE);
                    } else {
                        error_message.setVisibility(View.VISIBLE);
                        error_message.setText("No Appointment Found");
                        myRecyclerView.setVisibility(View.GONE);
                    }

                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    private void getTimeFromAndroid() {
        name.setText(savePref.getName());
        Glide.with(context).load(savePref.getImage()).into(profile_pic);

    /*    Date dt = new Date();
        int hours = dt.getHours();
        int min = dt.getMinutes();

        if (hours >= 1 || hours <= 12) {
            wish.setText("Good Morning");
        } else if (hours >= 12 || hours <= 16) {
            wish.setText("Good Afternoon");
        } else if (hours >= 16 || hours <= 21) {
            wish.setText("Good Evening");
        } else if (hours >= 21 || hours <= 24) {
            wish.setText("Good Night");
        }*/
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        APPOINTMENTLISTING_API(util.convertTimeStampDateTime(Long.parseLong(ts)));
    }

    private void setcalender(View view) {
        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                Log.e("date_selected", util.convertTimeStampDateTime(date.getTimeInMillis()/1000));

                APPOINTMENTLISTING_API(util.convertTimeStampDateTime(date.getTimeInMillis()/1000));
            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView,
                                         int dx, int dy) {

            }

            @Override
            public boolean onDateLongClicked(Calendar date, int position) {
                return true;
            }
        });

        getTimeFromAndroid();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}