package com.casecareapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.casecareapp.Activities.AddNoteActivity;
import com.casecareapp.Adapters.ClaimantsListAdapter;
import com.casecareapp.Adapters.NotesListAdapter;
import com.casecareapp.ModelClasses.ClaimantModel;
import com.casecareapp.Parser.AllAPIS;
import com.casecareapp.Parser.GetMethod;
import com.casecareapp.Parser.Message;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.Parameters;
import com.casecareapp.UtilFiles.SavePref;
import com.casecareapp.UtilFiles.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class NotesFragment extends Fragment {


    public NotesFragment() {
        // Required empty public constructor
    }

    Context context;
    private SavePref savePref;
    Unbinder unbinder;

    @BindView(R.id.search_bar)
    EditText search_bar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.add)
    ImageView add;

    NotesListAdapter adapter = null;
    ProgressDialog mDialog = null;
    private ArrayList<ClaimantModel> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notes, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AddNoteActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

        search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        NOTELISTING_API();
    }

    private void NOTELISTING_API() {
        util.hideKeyboard(getActivity());
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.NOTELISTING, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    JSONArray data = jsonMainobject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        ClaimantModel claimantModel = new ClaimantModel();
                        claimantModel.setId(object.getString("id"));
                        claimantModel.setDate(object.getString("date"));
                        claimantModel.setDescription(object.getString("description"));
                        claimantModel.setName(object.getJSONObject("claimant").getString("name"));
                        claimantModel.setImage(object.getJSONObject("claimant").getString("image"));
                        claimantModel.setUser_id(object.getJSONObject("claimant").getString("id"));
                        list.add(claimantModel);
                    }

                    if (list.size() > 0) {
                        adapter = new NotesListAdapter(context, list);
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(adapter);
                        myRecyclerView.setVisibility(View.VISIBLE);
                        error_message.setVisibility(View.GONE);
                    } else {
                        error_message.setVisibility(View.VISIBLE);
                        error_message.setText("No Notes Found");
                        myRecyclerView.setVisibility(View.GONE);
                    }
                } else {
                    util.IOSDialog(context, jsonMainobject.getString("error_message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}