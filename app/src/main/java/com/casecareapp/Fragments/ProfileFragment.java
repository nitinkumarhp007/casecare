package com.casecareapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.UpdateProfileActivity;
import com.casecareapp.MainActivity;
import com.casecareapp.R;
import com.casecareapp.UtilFiles.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }

    Context context;
    private SavePref savePref;

    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email_address)
    TextView email_address;
    @BindView(R.id.edit_profile)
    Button edit_profile;
    @BindView(R.id.phone)
    TextView phone;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email_address.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
        Glide.with(context).load(savePref.getImage()).into(profile_pic);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}