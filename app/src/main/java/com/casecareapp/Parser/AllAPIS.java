package com.casecareapp.Parser;


public class AllAPIS {


    //public static final String BASE_URL = "http://3.6.100.60:9222/api/";
    public static final String BASE_URL = "http://18.220.251.72:9000/api/";


    public static final String USERLOGIN = BASE_URL + "login";
    public static final String USER_SIGNUP = BASE_URL + "signup";
    public static final String VERIFY_OTP = BASE_URL + "forgotPassword";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "changePassword";
    public static final String EDIT_PROFILE = BASE_URL + "editProfile";
    public static final String TERMS = BASE_URL + "termsAndConditions";
    public static final String PRIVACYPOLICY = BASE_URL + "privacyPolicy";


    public static final String ADDCLAIMANT = BASE_URL + "addClaimant";
    public static final String EDITCLAIMANT = BASE_URL + "editClaimant";
    public static final String CLAIMANTLISTING = BASE_URL + "claimantListing";
    public static final String CLAIMANTDETAIL = BASE_URL + "claimantDetail";

    public static final String ADDNOTE = BASE_URL + "addNote";
    public static final String NOTELISTING = BASE_URL + "noteListing";
    public static final String EDITNOTE = BASE_URL + "editNote";
    public static final String NOTEDETAIL = BASE_URL + "noteDetail";

    public static final String ADDAPPOINTMENT = BASE_URL + "addAppointment";
    public static final String APPOINTMENTLISTING = BASE_URL + "appointmentListing";
    public static final String APPOINTMENTDETAIL = BASE_URL + "appointmentDetail";

    public static final String ADDDOCTOR = BASE_URL + "addDoctor";
    public static final String DOCTORLISTING = BASE_URL + "doctorListing";
    public static final String EDITDOCTOR = BASE_URL + "editDoctor";
    public static final String DOCTORDETAIL = BASE_URL + "doctorDetail";
    public static final String DELETEDOCTOR = BASE_URL + "deleteDoctor";

    public static final String ADDREMINDER = BASE_URL + "addReminder";
    public static final String REMINDERLISTING = BASE_URL + "reminderListing";
    public static final String EDITREMINDER = BASE_URL + "editReminder";
    public static final String REMINDERDETAIL = BASE_URL + "reminderDetail";

    public static final String ADDDOCUMENT = BASE_URL + "addDocument";
    public static final String DOCUMENTLISTING = BASE_URL + "documentListing";
    public static final String EDITDOCUMENT = BASE_URL + "editDocument";
    public static final String DOCUMENTDETAIL = BASE_URL + "documentDetail";
    public static final String DELETEDOCUMENT = BASE_URL + "deleteDocument";

    public static final String ADDREPORT = BASE_URL + "addReport";
    public static final String REPORTLISTING = BASE_URL + "reportListing";
    public static final String EDITREPORT = BASE_URL + "editReport";
    public static final String REPORTDETAIL = BASE_URL + "reportDetail";
    public static final String DELETEREPORT = BASE_URL + "deleteReport";

}
