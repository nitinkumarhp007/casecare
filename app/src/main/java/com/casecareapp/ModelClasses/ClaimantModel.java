package com.casecareapp.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class ClaimantModel implements Parcelable {
    String id = "";
    String image = "";
    String name = "";
    String user_id = "";
    String contactNumber = "";
    String ssn = "";
    String injuryDate = "";
    String date = "";
    String description = "";
    String examinerName = "";
    String examinerContactNumber = "";
    String examinerEmail = "";

    public ClaimantModel() {
    }


    protected ClaimantModel(Parcel in) {
        id = in.readString();
        image = in.readString();
        name = in.readString();
        user_id = in.readString();
        contactNumber = in.readString();
        ssn = in.readString();
        injuryDate = in.readString();
        date = in.readString();
        description = in.readString();
        examinerName = in.readString();
        examinerContactNumber = in.readString();
        examinerEmail = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(user_id);
        dest.writeString(contactNumber);
        dest.writeString(ssn);
        dest.writeString(injuryDate);
        dest.writeString(date);
        dest.writeString(description);
        dest.writeString(examinerName);
        dest.writeString(examinerContactNumber);
        dest.writeString(examinerEmail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ClaimantModel> CREATOR = new Creator<ClaimantModel>() {
        @Override
        public ClaimantModel createFromParcel(Parcel in) {
            return new ClaimantModel(in);
        }

        @Override
        public ClaimantModel[] newArray(int size) {
            return new ClaimantModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getInjuryDate() {
        return injuryDate;
    }

    public void setInjuryDate(String injuryDate) {
        this.injuryDate = injuryDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExaminerName() {
        return examinerName;
    }

    public void setExaminerName(String examinerName) {
        this.examinerName = examinerName;
    }

    public String getExaminerContactNumber() {
        return examinerContactNumber;
    }

    public void setExaminerContactNumber(String examinerContactNumber) {
        this.examinerContactNumber = examinerContactNumber;
    }

    public String getExaminerEmail() {
        return examinerEmail;
    }

    public void setExaminerEmail(String examinerEmail) {
        this.examinerEmail = examinerEmail;
    }

    public static Creator<ClaimantModel> getCREATOR() {
        return CREATOR;
    }
}
