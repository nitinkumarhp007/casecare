package com.casecareapp.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class DoctorModel implements Parcelable {
    String id="";
    String image="";
    String name="";
    String phone="";
    String fax_number="";
    String email_address="";
    String doctor_address="";
    String latitude="";
    String logitude="";

    public DoctorModel()
    {

    }

    protected DoctorModel(Parcel in) {
        id = in.readString();
        image = in.readString();
        name = in.readString();
        phone = in.readString();
        fax_number = in.readString();
        email_address = in.readString();
        doctor_address = in.readString();
        latitude = in.readString();
        logitude = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(fax_number);
        dest.writeString(email_address);
        dest.writeString(doctor_address);
        dest.writeString(latitude);
        dest.writeString(logitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DoctorModel> CREATOR = new Creator<DoctorModel>() {
        @Override
        public DoctorModel createFromParcel(Parcel in) {
            return new DoctorModel(in);
        }

        @Override
        public DoctorModel[] newArray(int size) {
            return new DoctorModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax_number() {
        return fax_number;
    }

    public void setFax_number(String fax_number) {
        this.fax_number = fax_number;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getDoctor_address() {
        return doctor_address;
    }

    public void setDoctor_address(String doctor_address) {
        this.doctor_address = doctor_address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogitude() {
        return logitude;
    }

    public void setLogitude(String logitude) {
        this.logitude = logitude;
    }
}
