package com.casecareapp.ModelClasses;

public class ItemModel {
    String text="";
    String id="";
    boolean is_selected=false;

    public ItemModel(String text, String id, boolean is_selected) {
        this.text = text;
        this.id = id;
        this.is_selected = is_selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
