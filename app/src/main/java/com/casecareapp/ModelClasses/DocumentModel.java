package com.casecareapp.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class DocumentModel implements Parcelable {

    String id="";
    String title="";
    String description="";
    String claimantId="";
    String claimantname="";
    String image="";
    String doc_image="";

    public DocumentModel()
    {}

    protected DocumentModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        claimantId = in.readString();
        claimantname = in.readString();
        image = in.readString();
        doc_image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(claimantId);
        dest.writeString(claimantname);
        dest.writeString(image);
        dest.writeString(doc_image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DocumentModel> CREATOR = new Creator<DocumentModel>() {
        @Override
        public DocumentModel createFromParcel(Parcel in) {
            return new DocumentModel(in);
        }

        @Override
        public DocumentModel[] newArray(int size) {
            return new DocumentModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClaimantId() {
        return claimantId;
    }

    public void setClaimantId(String claimantId) {
        this.claimantId = claimantId;
    }

    public String getClaimantname() {
        return claimantname;
    }

    public void setClaimantname(String claimantname) {
        this.claimantname = claimantname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDoc_image() {
        return doc_image;
    }

    public void setDoc_image(String doc_image) {
        this.doc_image = doc_image;
    }
}
