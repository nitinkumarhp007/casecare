package com.casecareapp.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportModel implements Parcelable {

    String image="";
    String id="";
    String title="";
    String claimantId="";
    String claimantname="";
    String goals_for_referral="";
    String provider="";
    String litigation="";
    String mechanism_of_injury="";
    String accepted_body_parts="";
    String diagnosis="";
    String surgeries="";
    String created="";

    public ReportModel()
    {}

    protected ReportModel(Parcel in) {
        image = in.readString();
        id = in.readString();
        title = in.readString();
        claimantId = in.readString();
        claimantname = in.readString();
        goals_for_referral = in.readString();
        provider = in.readString();
        litigation = in.readString();
        mechanism_of_injury = in.readString();
        accepted_body_parts = in.readString();
        diagnosis = in.readString();
        surgeries = in.readString();
        created = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(claimantId);
        dest.writeString(claimantname);
        dest.writeString(goals_for_referral);
        dest.writeString(provider);
        dest.writeString(litigation);
        dest.writeString(mechanism_of_injury);
        dest.writeString(accepted_body_parts);
        dest.writeString(diagnosis);
        dest.writeString(surgeries);
        dest.writeString(created);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReportModel> CREATOR = new Creator<ReportModel>() {
        @Override
        public ReportModel createFromParcel(Parcel in) {
            return new ReportModel(in);
        }

        @Override
        public ReportModel[] newArray(int size) {
            return new ReportModel[size];
        }
    };

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClaimantId() {
        return claimantId;
    }

    public void setClaimantId(String claimantId) {
        this.claimantId = claimantId;
    }

    public String getClaimantname() {
        return claimantname;
    }

    public void setClaimantname(String claimantname) {
        this.claimantname = claimantname;
    }

    public String getGoals_for_referral() {
        return goals_for_referral;
    }

    public void setGoals_for_referral(String goals_for_referral) {
        this.goals_for_referral = goals_for_referral;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getLitigation() {
        return litigation;
    }

    public void setLitigation(String litigation) {
        this.litigation = litigation;
    }

    public String getMechanism_of_injury() {
        return mechanism_of_injury;
    }

    public void setMechanism_of_injury(String mechanism_of_injury) {
        this.mechanism_of_injury = mechanism_of_injury;
    }

    public String getAccepted_body_parts() {
        return accepted_body_parts;
    }

    public void setAccepted_body_parts(String accepted_body_parts) {
        this.accepted_body_parts = accepted_body_parts;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getSurgeries() {
        return surgeries;
    }

    public void setSurgeries(String surgeries) {
        this.surgeries = surgeries;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
