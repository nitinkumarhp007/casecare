package com.casecareapp.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class AppointmentModel implements Parcelable {
    String id="";
    String doctorId="";
    String doctorname="";
    String date="";
    String time="";
    String name="";
    String contactNumber="";
    String address="";
    String latitude="";
    String longitude="";
    String image="";
    String c_name="";
    String c_dob="";
    String c_ssn="";
    String c_id="";
    String c_user_id="";
    String c_image="";

    public AppointmentModel()
    {}

    protected AppointmentModel(Parcel in) {
        id = in.readString();
        doctorId = in.readString();
        doctorname = in.readString();
        date = in.readString();
        time = in.readString();
        name = in.readString();
        contactNumber = in.readString();
        address = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        image = in.readString();
        c_name = in.readString();
        c_dob = in.readString();
        c_ssn = in.readString();
        c_id = in.readString();
        c_user_id = in.readString();
        c_image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(doctorId);
        dest.writeString(doctorname);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(name);
        dest.writeString(contactNumber);
        dest.writeString(address);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(image);
        dest.writeString(c_name);
        dest.writeString(c_dob);
        dest.writeString(c_ssn);
        dest.writeString(c_id);
        dest.writeString(c_user_id);
        dest.writeString(c_image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AppointmentModel> CREATOR = new Creator<AppointmentModel>() {
        @Override
        public AppointmentModel createFromParcel(Parcel in) {
            return new AppointmentModel(in);
        }

        @Override
        public AppointmentModel[] newArray(int size) {
            return new AppointmentModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_dob() {
        return c_dob;
    }

    public void setC_dob(String c_dob) {
        this.c_dob = c_dob;
    }

    public String getC_ssn() {
        return c_ssn;
    }

    public void setC_ssn(String c_ssn) {
        this.c_ssn = c_ssn;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getC_user_id() {
        return c_user_id;
    }

    public void setC_user_id(String c_user_id) {
        this.c_user_id = c_user_id;
    }

    public String getC_image() {
        return c_image;
    }

    public void setC_image(String c_image) {
        this.c_image = c_image;
    }
}
