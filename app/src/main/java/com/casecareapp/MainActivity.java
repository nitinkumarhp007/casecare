package com.casecareapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.casecareapp.Activities.AddDoctorActivity;
import com.casecareapp.Activities.ClaimantsListActivity;
import com.casecareapp.Activities.DoctorListActivity;
import com.casecareapp.Activities.DocumentListActivity;
import com.casecareapp.Activities.ForgotPasswordActivity;
import com.casecareapp.Activities.GuidelinesActivity;
import com.casecareapp.Activities.ReportListActivity;
import com.casecareapp.Activities.SettingActivity;
import com.casecareapp.Activities.SignUpActivity;
import com.casecareapp.Activities.UpdateProfileActivity;
import com.casecareapp.Fragments.HomeFragment;
import com.casecareapp.Fragments.NotesFragment;
import com.casecareapp.Fragments.ProfileFragment;
import com.casecareapp.Fragments.ReminderFragment;
import com.casecareapp.UtilFiles.SavePref;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity {

    MainActivity context;
    private SavePref savePref;
    BottomNavigationView navigation;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.my_drawer_layout)
    DrawerLayout myDrawerLayout;
    @BindView(R.id.claimants)
    Button claimants;
    @BindView(R.id.doctors)
    Button doctors;
    @BindView(R.id.documents)
    Button documents;
    @BindView(R.id.reports)
    Button reports;
    @BindView(R.id.guidelines)
    Button guidelines;
    @BindView(R.id.tutorials)
    Button tutorials;
    @BindView(R.id.settings)
    Button settings;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email_address)
    TextView email_address;
    @BindView(R.id.edit_profile)
    TextView edit_profile;

    String notification_code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ButterKnife.bind(this);

        savePref = new SavePref(context);

        Log.e("auth_key", savePref.getID());
        Log.e("auth_key", SavePref.getDeviceToken(this, "token"));
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        navigation.setSelectedItemId(R.id.navigation_home);


        Log.e("token____", SavePref.getDeviceToken(this, "token"));

        /*if (Environment.isExternalStorageManager()) {
            //todo when permission is granted
        } else {
            //request for the permission
            Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        }
*/

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDrawerLayout.openDrawer(Gravity.RIGHT, true);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email_address.setText(savePref.getEmail());
        Glide.with(context).load(savePref.getImage()).into(image);
    }


    private boolean checkPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int result = ContextCompat.checkSelfPermission(MainActivity.this, READ_EXTERNAL_STORAGE);
            int result1 = ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_notes:
                    loadFragment(new NotesFragment());
                    return true;
                case R.id.navigation_reminder:
                    loadFragment(new ReminderFragment());
                    return true;
                case R.id.navigation_my_profile:
                    loadFragment(new ProfileFragment());
                    return true;
            }
            return false;
        }
    };

    @OnClick({R.id.claimants, R.id.doctors, R.id.documents, R.id.reports,
            R.id.guidelines, R.id.edit_profile, R.id.tutorials, R.id.settings})
    public void onViewClicked(View view) {
        myDrawerLayout.closeDrawer(Gravity.RIGHT, false);
        switch (view.getId()) {
            case R.id.claimants:
                startActivity(new Intent(context, ClaimantsListActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.doctors:
                startActivity(new Intent(context, DoctorListActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.documents:
                startActivity(new Intent(context, DocumentListActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.reports:
                startActivity(new Intent(context, ReportListActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.guidelines:
                startActivity(new Intent(context, GuidelinesActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile:
                startActivity(new Intent(context, UpdateProfileActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.tutorials:

                break;
            case R.id.settings:
                startActivity(new Intent(context, SettingActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }


}